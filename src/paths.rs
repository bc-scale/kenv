use crate::prelude::*;
use std::path::{Path, PathBuf};
use uuid::Uuid;

pub fn mkdir_bin() -> AppResult<PathBuf> {
  if let Some(dir) = dirs::data_local_dir() {
    let path = dir.join(PKG_NAME).join("bin");
    if !path.exists() {
      std::fs::create_dir_all(&path)?;
    }
    Ok(path)
  } else {
    err(ErrorKind::NotFound, "bin path not found")
  }
}

pub fn mkdir_downloads<S: AsRef<str>>(
  name: S,
  version: S,
) -> AppResult<PathBuf> {
  let path = mkdir_bin()?
    .join("downloads")
    .join(name.as_ref())
    .join(version.as_ref());
  if !path.exists() {
    std::fs::create_dir_all(&path)?;
  }
  Ok(path)
}

pub fn gen_temp_filepath<S: AsRef<str>>(extension: S) -> PathBuf {
  let mut path = std::env::temp_dir();
  path.push(format!("{}.{}", Uuid::new_v4(), extension.as_ref()));
  path
}

pub fn mktemp() -> AppResult<PathBuf> {
  let mut path = std::env::temp_dir();
  path.push(format!("{}", Uuid::new_v4()));
  if !path.exists() {
    std::fs::create_dir_all(&path)?;
  }
  Ok(path)
}

pub fn rmdir(path: &Path) -> AppResult<()> {
  std::fs::remove_dir_all(path)?;
  Ok(())
}
