use crate::{
  prelude::*,
  tools::{k9s::K9s, kind::Kind},
};

pub fn view(name: String) -> AppResult<()> {
  let clusters = Kind::list_clusters()?;
  if !clusters.contains(&name) {
    return err(ErrorKind::NotFound, "supplied cluster does not exist");
  }
  let context = Kind::cluster_name_to_context(name.as_str());
  let mut command = K9s::mk_command(&K9s {})?;
  command
    .arg("--context")
    .arg(context.as_str())
    .arg("--request-timeout")
    .arg("10s")
    .arg("--headless")
    .arg("--command")
    .arg("namespaces")
    .output()?;
  Ok(())
}
