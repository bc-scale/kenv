use crate::prelude::*;
use crate::tools::kind::Kind;

pub fn list_versions(top: usize) -> AppResult<()> {
  let versions = Kind::list_supported_k8s_versions()?;
  Tui::chapter("Supported Kubernetes versions");
  for v in versions.iter().take(top) {
    let version = v.to_string();
    Tui::writeln(Text::ok(">>", version.as_str(), "|"), None);
  }
  Tui::divider();
  Ok(())
}
