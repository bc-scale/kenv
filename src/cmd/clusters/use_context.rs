use crate::{
  prelude::*,
  tools::{kind::Kind, Kubectl},
};

pub fn use_context(name: String) -> AppResult<()> {
  let context = Kind::cluster_name_to_context(name.as_str());
  Kubectl::use_context(context.as_str())
}
