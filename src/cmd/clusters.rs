mod down;
mod list;
mod list_versions;
mod up;
mod use_context;
mod view;

use crate::prelude::*;
use crate::tools::kind::Kind;
use clap::{Arg, ArgMatches, SubCommand};
use down::*;
use list::*;
use list_versions::*;
use std::fmt::Display;
use std::path::PathBuf;
use std::str::FromStr;
use up::*;
use use_context::*;
use view::*;

enum ClustersCmdArgs {
  Up(ClusterStartArgs),
  Down(ClusterStopArgs),
  List,
  ListVersions(usize),
  UseContext(String),
  View(String),
}

pub struct ClustersCmd {}

const CLUSTERS: &str = "clusters";
const CLUSTERS_UP: &str = "up";
const CLUSTERS_UP_CLUSTER_NAME: &str = "cluster-name";
const CLUSTERS_UP_VERSION: &str = "version";
const CLUSTERS_UP_WORKERS: &str = "workers";
const CLUSTERS_UP_ENABLE_CALICO_CNI: &str = "enable-calico-cni";
const CLUSTERS_UP_ENABLE_IMAGE_REGISTRY: &str = "enable-image-registry";
const CLUSTERS_UP_IMAGE_REGISTRY_PORT: &str = "image-registry-port";
const CLUSTERS_UP_IMAGE_REGISTRY_UI_PORT: &str = "image-registry-ui-port";
const CLUSTERS_UP_EXPOSE_PORT: &str = "expose-port";
const CLUSTERS_UP_MOUNT_VOLUME: &str = "mount-volume";
const CLUSTERS_UP_CUSTOM_REGISTRY: &str = "custom-registry";
const CLUSTERS_DOWN: &str = "down";
const CLUSTERS_DOWN_CLUSTER_NAME: &str = "cluster-name";
const CLUSTERS_LIST: &str = "list";
const CLUSTERS_LIST_VERSIONS: &str = "list-versions";
const CLUSTERS_LIST_VERSIONS_TOP: &str = "top";
const CLUSTERS_USE: &str = "use";
const CLUSTERS_USE_CLUSTER_NAME: &str = "cluster-name";
const CLUSTERS_VIEW: &str = "view";
const CLUSTERS_VIEW_CLUSTER_NAME: &str = "cluster-name";

impl cmd::Command for ClustersCmd {
  fn spec<'a>(&self) -> clap::App<'a, 'a> {
    SubCommand::with_name(CLUSTERS)
      .about("Manage Kubernetes clusters with KinD")
      .aliases(&["c", "cl", "clus", "clust", "cluster"])
      .subcommand(
        SubCommand::with_name(CLUSTERS_UP)
          .about("Starts up Kubernetes cluster")
          .settings(CMD_SETTINGS.as_slice())
          .args(&[
            Arg::with_name(CLUSTERS_UP_CLUSTER_NAME)
              .required(false)
              .takes_value(true)
              .long(CLUSTERS_UP_CLUSTER_NAME)
              .short("n")
              .validator(validate_cluster_name)
              .default_value("local")
              .help("The name of the cluster to spin up"),
            Arg::with_name(CLUSTERS_UP_VERSION)
              .required(false)
              .takes_value(true)
              .long(CLUSTERS_UP_VERSION)
              .short("v")
              .validator(validate_cluster_version)
              .default_value("latest")
              .help("The kubernetes version"),
            Arg::with_name(CLUSTERS_UP_WORKERS)
              .required(false)
              .takes_value(true)
              .long(CLUSTERS_UP_WORKERS)
              .short("w")
              .validator(validate_non_negative::<u8>)
              .default_value("0")
              .help("The number of worker nodes"),
            Arg::with_name(CLUSTERS_UP_ENABLE_CALICO_CNI)
              .required(false)
              .long(CLUSTERS_UP_ENABLE_CALICO_CNI)
              .short("c")
              .help("Enables Calico CNI"),
            Arg::with_name(CLUSTERS_UP_ENABLE_IMAGE_REGISTRY)
              .required(false)
              .long(CLUSTERS_UP_ENABLE_IMAGE_REGISTRY)
              .short("i")
              .help("Enables container image registry"),
            Arg::with_name(CLUSTERS_UP_IMAGE_REGISTRY_PORT)
              .required(false)
              .long(CLUSTERS_UP_IMAGE_REGISTRY_PORT)
              .takes_value(true)
              .default_value("5000")
              .validator(validate_positive::<u16>)
              .help("The port where container image registry is exposed"),
            Arg::with_name(CLUSTERS_UP_IMAGE_REGISTRY_UI_PORT)
              .required(false)
              .long(CLUSTERS_UP_IMAGE_REGISTRY_UI_PORT)
              .takes_value(true)
              .default_value("5001")
              .validator(validate_positive::<u16>)
              .help("The port where container image registry UI is exposed"),
            Arg::with_name(CLUSTERS_UP_EXPOSE_PORT)
              .required(false)
              .long(CLUSTERS_UP_EXPOSE_PORT)
              .short("p")
              .multiple(true)
              .takes_value(true)
              .validator(validate_port_mapping)
              .help("The port to expose with format host_port:container_port"),
            Arg::with_name(CLUSTERS_UP_MOUNT_VOLUME)
              .required(false)
              .long(CLUSTERS_UP_MOUNT_VOLUME)
              .short("m")
              .multiple(true)
              .takes_value(true)
              .validator(validate_volume_mount)
              .help("The volume mount with format host_path:container_path"),
            Arg::with_name(CLUSTERS_UP_CUSTOM_REGISTRY)
              .required(false)
              .takes_value(true)
              .long(CLUSTERS_UP_CUSTOM_REGISTRY)
              .help("Custom container registry fot kindest/node images"),
          ]),
      )
      .subcommand(
        SubCommand::with_name(CLUSTERS_DOWN)
          .about("Delete KinD cluster")
          .alias("stop")
          .settings(CMD_SETTINGS.as_slice())
          .arg(
            Arg::with_name(CLUSTERS_DOWN_CLUSTER_NAME)
              .required(false)
              .takes_value(true)
              .long(CLUSTERS_DOWN_CLUSTER_NAME)
              .short("n")
              .validator(validate_cluster_name)
              .default_value("local")
              .help("The name of the cluster"),
          ),
      )
      .subcommand(
        SubCommand::with_name(CLUSTERS_LIST)
          .about("List managed KinD clusters")
          .alias("ls")
          .settings(CMD_SETTINGS.as_slice()),
      )
      .subcommand(
        SubCommand::with_name(CLUSTERS_LIST_VERSIONS)
          .about("List Kubernetes versions supported")
          .alias("lsver")
          .settings(CMD_SETTINGS.as_slice())
          .arg(
            Arg::with_name(CLUSTERS_LIST_VERSIONS_TOP)
              .takes_value(true)
              .default_value("10")
              .long(CLUSTERS_LIST_VERSIONS_TOP)
              .short("t")
              .validator(validate_positive::<usize>)
              .help("The maximum number of versions to return"),
          ),
      )
      .subcommand(
        SubCommand::with_name(CLUSTERS_USE)
          .about("Change current context")
          .alias("switch")
          .settings(CMD_SETTINGS.as_slice())
          .arg(
            Arg::with_name(CLUSTERS_USE_CLUSTER_NAME)
              .required(true)
              .takes_value(true)
              .long(CLUSTERS_USE_CLUSTER_NAME)
              .short("n")
              .validator(validate_cluster_name)
              .help("The name of the cluster"),
          ),
      )
      .subcommand(
        SubCommand::with_name(CLUSTERS_VIEW)
          .about("View cluster using excellent k9s")
          .aliases(&["v", "nav", "navig", "navigate"])
          .settings(CMD_SETTINGS.as_slice())
          .arg(
            Arg::with_name(CLUSTERS_VIEW_CLUSTER_NAME)
              .required(false)
              .takes_value(true)
              .long(CLUSTERS_VIEW_CLUSTER_NAME)
              .short("n")
              .validator(validate_cluster_name)
              .default_value("local")
              .help("The name of the cluster"),
          ),
      )
  }

  fn run(&self, matches: &ArgMatches) -> Option<AppResult<()>> {
    if let Some(args) = self.match_args(matches) {
      return match args {
        ClustersCmdArgs::Up(args) => Some(up(&args)),
        ClustersCmdArgs::Down(args) => Some(down(&args)),
        ClustersCmdArgs::List => Some(list()),
        ClustersCmdArgs::ListVersions(top) => Some(list_versions(top)),
        ClustersCmdArgs::UseContext(name) => Some(use_context(name)),
        ClustersCmdArgs::View(name) => Some(view(name)),
      };
    }
    None
  }
}

impl ClustersCmd {
  fn match_args(&self, matches: &ArgMatches) -> Option<ClustersCmdArgs> {
    matches
      .subcommand_matches(CLUSTERS)
      .map(|matches| {
        if let Some(m) = matches.subcommand_matches(CLUSTERS_UP) {
          let name = m.value_of(CLUSTERS_UP_CLUSTER_NAME).unwrap().to_owned();
          let version = m.value_of(CLUSTERS_UP_VERSION).unwrap().to_owned();
          let version = Kind::resolve_k8s_version(version.as_str()).unwrap();
          let workers = m.value_of(CLUSTERS_UP_WORKERS).unwrap();
          let workers = workers.parse::<u8>().unwrap();
          let enable_calico_cni = m.is_present(CLUSTERS_UP_ENABLE_CALICO_CNI);
          let enable_image_registry =
            m.is_present(CLUSTERS_UP_ENABLE_IMAGE_REGISTRY);
          let reg_port = m
            .value_of(CLUSTERS_UP_IMAGE_REGISTRY_PORT)
            .unwrap()
            .parse::<u16>()
            .unwrap();
          let reg_ui_port = m
            .value_of(CLUSTERS_UP_IMAGE_REGISTRY_UI_PORT)
            .unwrap()
            .parse::<u16>()
            .unwrap();
          let ports = m
            .values_of(CLUSTERS_UP_EXPOSE_PORT)
            .unwrap_or_default()
            .filter_map(parse_port_mapping)
            .collect::<Vec<(u16, u16)>>();
          let mounts = m
            .values_of(CLUSTERS_UP_MOUNT_VOLUME)
            .unwrap_or_default()
            .filter_map(parse_volume_mount)
            .collect::<Vec<(PathBuf, PathBuf)>>();
          let custom_registry = m
            .value_of(CLUSTERS_UP_CUSTOM_REGISTRY)
            .map(|x| x.to_owned());
          let args = ClusterStartArgs {
            name: name.to_owned(),
            version,
            workers,
            enable_calico_cni,
            enable_image_registry,
            image_registry_name: format!("{}-registry", &name),
            image_registry_port: reg_port,
            image_registry_ui_name: format!("{}-registry-ui", &name),
            image_registry_ui_port: reg_ui_port,
            exposed_ports: ports,
            mounts,
            custom_registry,
          };
          return Some(ClustersCmdArgs::Up(args));
        }
        if let Some(matches) = matches.subcommand_matches(CLUSTERS_DOWN) {
          let name = matches
            .value_of(CLUSTERS_DOWN_CLUSTER_NAME)
            .unwrap()
            .to_owned();
          let args = ClusterStopArgs {
            name: name.to_owned(),
            registry_name: format!("{}-registry", &name),
            registry_ui_name: format!("{}-registry-ui", &name),
          };
          return Some(ClustersCmdArgs::Down(args));
        }
        if matches.subcommand_matches(CLUSTERS_LIST).is_some() {
          return Some(ClustersCmdArgs::List);
        }
        if let Some(matches) =
          matches.subcommand_matches(CLUSTERS_LIST_VERSIONS)
        {
          let top = matches
            .value_of(CLUSTERS_LIST_VERSIONS_TOP)
            .unwrap()
            .parse::<usize>()
            .unwrap();
          return Some(ClustersCmdArgs::ListVersions(top));
        }
        if let Some(matches) = matches.subcommand_matches(CLUSTERS_USE) {
          let name = matches
            .value_of(CLUSTERS_USE_CLUSTER_NAME)
            .unwrap()
            .to_owned();
          return Some(ClustersCmdArgs::UseContext(name));
        }
        if let Some(matches) = matches.subcommand_matches(CLUSTERS_VIEW) {
          let name = matches
            .value_of(CLUSTERS_VIEW_CLUSTER_NAME)
            .unwrap()
            .to_owned();
          return Some(ClustersCmdArgs::View(name));
        }
        None
      })
      .flatten()
  }
}

fn validate_cluster_name(input: String) -> Result<(), String> {
  if input.is_empty() {
    return Err("cluster name cannot be empty".to_owned());
  }
  Ok(())
}

fn validate_cluster_version(input: String) -> Result<(), String> {
  if input.is_empty() {
    return Err("cluster version cannot be empty".to_owned());
  }
  Ok(())
}

fn validate_positive<T>(input: String) -> Result<(), String>
where
  T: FromStr + Ord + Default,
  <T as FromStr>::Err: Display,
{
  match input.parse::<T>() {
    Ok(x) => {
      if x > T::default() {
        Ok(())
      } else {
        Err("number should be strictly positive".to_owned())
      }
    }
    Err(err) => Err(format!("{}", &err)),
  }
}

fn validate_non_negative<T>(input: String) -> Result<(), String>
where
  T: FromStr + Ord + Default,
  <T as FromStr>::Err: Display,
{
  match input.parse::<T>() {
    Ok(value) => {
      if value >= T::default() {
        Ok(())
      } else {
        Err("supplied number cannot be zero or negative".to_owned())
      }
    }
    Err(err) => Err(format!("{}", &err)),
  }
}

fn validate_port_mapping(input: String) -> Result<(), String> {
  match parse_port_mapping(input.as_ref()) {
    None => Err("cannot parse port mapping".to_owned()),
    Some(_) => Ok(()),
  }
}

fn parse_port_mapping(input: &str) -> Option<(u16, u16)> {
  let parts = input.trim().split(':').collect::<Vec<&str>>();
  if parts.len() != 2 {
    return None;
  }
  let host_port = parts[0].parse::<u16>();
  let container_port = parts[1].parse::<u16>();
  if let Ok(host_port) = host_port {
    if let Ok(container_port) = container_port {
      return Some((host_port, container_port));
    }
  }
  None
}

fn validate_volume_mount(input: String) -> Result<(), String> {
  match parse_volume_mount(input.as_ref()) {
    None => Err("cannot parse volume mount".to_owned()),
    Some(_) => Ok(()),
  }
}

fn parse_volume_mount(input: &str) -> Option<(PathBuf, PathBuf)> {
  let parts = input.trim().split(':').collect::<Vec<&str>>();
  if parts.len() != 2 {
    return None;
  }
  let host_path = PathBuf::from(parts[0]);
  let container_path = PathBuf::from(parts[1]);
  Some((host_path, container_path))
}
