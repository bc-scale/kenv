use crate::prelude::*;

pub fn list() -> AppResult<()> {
  Tui::chapter("Tools");
  let mut tools = tools::all();
  for tool in &mut tools {
    let path = tool.get_path()?;
    let path = path.to_str().unwrap_or("<unknown_path>");
    Tui::write(Text::ok(">>", path, "|"), None);
    // Tui::write(Text::ok(">>", tool.get_name(), "|"), None);
    if !tool.can_download() {
      if let Ok(version) = tool.get_version() {
        Tui::grayln(version);
      } else {
        Tui::redln("unknown");
      }
      continue;
    }
    let latest = tool.get_latest_version().unwrap_or_default();
    if let Ok(version) = tool.get_version() {
      if version != latest {
        Tui::yellow(version);
        Tui::suffix("<<");
        Tui::yellowln(latest);
      } else {
        Tui::greenln(version);
      }
    } else if latest.is_empty() {
      Tui::redln("unknown");
    } else {
      Tui::red("unknown");
      Tui::suffix("<<");
      Tui::yellowln(latest);
    }
  }
  Tui::divider();
  Ok(())
}
