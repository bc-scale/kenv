pub use std::io::ErrorKind;

pub type AppResult<T> = Result<T, Box<dyn std::error::Error>>;

pub fn err<T, S: AsRef<str>>(kind: ErrorKind, msg: S) -> AppResult<T> {
  Err(Box::new(std::io::Error::new(kind, msg.as_ref())))
}
