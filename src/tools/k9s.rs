use crate::prelude::*;

pub struct K9s {}

impl tools::Tool for K9s {
  fn can_download(&self) -> bool {
    true
  }

  fn get_name(&self) -> &'static str {
    "k9s"
  }

  fn get_latest_version_url(&self) -> &'static str {
    "https://github.com/derailed/k9s/releases/latest"
  }

  fn get_download_info(&self) -> Option<(&'static str, Option<&'static str>)> {
    match std::env::consts::OS {
      "linux" => Some(("https://github.com/derailed/k9s/releases/download/v{}/k9s_Linux_x86_64.tar.gz", None)),
      "windows" => Some(("https://github.com/derailed/k9s/releases/download/v{}/k9s_Windows_x86_64.tar.gz", None)),
      _ => None,
    }
  }

  fn get_version(&self) -> AppResult<String> {
    let output = self.mk_command()?.arg("version").arg("--short").output()?;
    let output = String::from_utf8(output.stdout)?;
    let version = output
      .trim_end()
      .lines()
      .next()
      .unwrap()
      .split_ascii_whitespace()
      .last()
      .unwrap()
      .trim_start_matches('v');
    Ok(version.to_owned())
  }
}
