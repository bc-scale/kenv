use crate::prelude::*;
use serde::{Deserialize, Serialize};
use std::cmp::Ordering;
use std::fs::File;
use std::io::{BufRead, BufReader, Write};
use std::thread::spawn;

pub struct Kind {}

impl tools::Tool for Kind {
  fn can_download(&self) -> bool {
    true
  }

  fn get_name(&self) -> &'static str {
    "kind"
  }

  fn get_latest_version_url(&self) -> &'static str {
    "https://github.com/kubernetes-sigs/kind/releases/latest"
  }

  fn get_download_info(&self) -> Option<(&'static str, Option<&'static str>)> {
    match std::env::consts::OS {
      "linux" => Some(("https://github.com/kubernetes-sigs/kind/releases/download/v{}/kind-linux-amd64", None)),
      "windows" => Some(("https://github.com/kubernetes-sigs/kind/releases/download/v{}/kind-windows-amd64", None)),
      _ => None,
    }
  }

  fn get_version(&self) -> AppResult<String> {
    let output = self.mk_command()?.arg("version").arg("--quiet").output()?;
    let output = String::from_utf8(output.stdout)?;
    let version = output.trim_end().to_owned();
    Ok(version)
  }
}

#[derive(Debug)]
pub struct ClusterVersion {
  pub major: u16,
  pub minor: u16,
  pub build: u16,
}

impl ToString for ClusterVersion {
  fn to_string(&self) -> String {
    format!("v{}.{}.{}", &self.major, &self.minor, &self.build)
  }
}

#[derive(Debug, Serialize, Deserialize)]
struct TagSpec {
  pub name: String,
}

#[derive(Debug, Serialize, Deserialize)]
struct ImageSpec {
  pub count: u32,
  pub results: Vec<TagSpec>,
}

const KINDEST_NODE_TAGS_URL: &str = "https://registry.hub.docker.com/v2/repositories/kindest/node/tags?page_size=1024";

fn parse_version(version: &str) -> Option<ClusterVersion> {
  let parts = version
    .trim_start_matches('v')
    .split('.')
    .filter_map(|x| x.parse::<u16>().ok())
    .collect::<Vec<u16>>();
  if parts.len() != 3 {
    return None;
  }
  Some(ClusterVersion {
    major: parts[0],
    minor: parts[1],
    build: parts[2],
  })
}

fn compare_versions(v1: &ClusterVersion, v2: &ClusterVersion) -> Ordering {
  let major = v2.major.cmp(&v1.major);
  if !major.is_eq() {
    return major;
  }
  let minor = v2.minor.cmp(&v1.minor);
  if !minor.is_eq() {
    return minor;
  }
  v2.build.cmp(&v1.build)
}

impl Kind {
  pub fn cluster_name_to_context<S: AsRef<str>>(cluster_name: S) -> String {
    format!("kind-{}", cluster_name.as_ref())
  }

  pub fn list_supported_k8s_versions() -> AppResult<Vec<ClusterVersion>> {
    let response = curl(KINDEST_NODE_TAGS_URL, None)?;
    let status = response.status();
    if !status.is_success() {
      return err(
        ErrorKind::Other,
        "cannot fetch supported Kubernetes versions",
      );
    }
    let response = response.text()?;
    let spec: ImageSpec = serde_json::from_str(response.as_str())?;
    let mut versions = spec
      .results
      .iter()
      .filter_map(|x| parse_version(&x.name))
      .collect::<Vec<ClusterVersion>>();
    versions.sort_by(compare_versions);
    Ok(versions)
  }

  pub fn resolve_k8s_version<S: AsRef<str>>(version: S) -> AppResult<String> {
    let mut result = version.as_ref().to_owned();
    if version.as_ref() == "latest" {
      if let Some(version) = Self::list_supported_k8s_versions()?.first() {
        result = version.to_string();
      } else {
        return err(ErrorKind::NotFound, "cannot find latest version");
      }
    }
    result = result.trim_start_matches('v').to_owned();
    Ok(result)
  }

  pub fn format_image<S: AsRef<str>>(custom_registry: S, version: S) -> String {
    format!(
      "{}kindest/node:v{}",
      custom_registry.as_ref(),
      version.as_ref()
    )
  }

  pub fn list_clusters() -> AppResult<Vec<String>> {
    let output = Self::mk_command(&Self {})?
      .arg("get")
      .arg("clusters")
      .output()?;
    let output = String::from_utf8(output.stdout)?;
    let names = output
      .split('\n')
      .filter(|x| !x.is_empty())
      .map(|x| x.to_owned())
      .collect::<Vec<String>>();
    Ok(names)
  }

  pub fn start<S: AsRef<str>>(
    cluster_name: S,
    version: S,
    custom_registry: S,
    config: S,
    verbose: bool,
  ) -> AppResult<()> {
    let version = Self::resolve_k8s_version(version.as_ref())?;
    let config_path = paths::gen_temp_filepath("yaml");
    let mut file = File::create(&config_path)?;
    file.write_all(config.as_ref().as_bytes())?;
    let mut proc = Self::mk_command(&Self {})?
      .stdout(std::process::Stdio::piped())
      .stderr(std::process::Stdio::piped())
      .arg("create")
      .arg("cluster")
      .arg("--name")
      .arg(cluster_name.as_ref())
      .arg("--image")
      .arg(Self::format_image(
        custom_registry.as_ref(),
        version.as_str(),
      ))
      .arg("--config")
      .arg(config_path)
      .spawn()?;
    let stdout_reader = BufReader::new(proc.stdout.take().unwrap());
    let stderr_reader = BufReader::new(proc.stderr.take().unwrap());
    let error_sink = spawn(move || {
      stderr_reader
        .lines()
        .for_each(Self::kind_create_logger(verbose))
    });
    stdout_reader
      .lines()
      .for_each(Self::kind_create_logger(verbose));
    error_sink.join().unwrap();
    let status = proc.wait().unwrap();
    if !status.success() {
      return err(ErrorKind::Other, "failed to start cluster");
    }
    Ok(())
  }

  fn kind_create_logger(verbose: bool) -> fn(Result<String, std::io::Error>) {
    if !verbose {
      return |_| {};
    }
    |line: Result<String, std::io::Error>| {
      let line = line.unwrap_or_default();
      if !line.is_empty() && !line.contains("...") && line.starts_with(' ') {
        let line = line.as_str().trim();
        let prefix = line.chars().next().unwrap().to_string();
        let suffix = line.chars().nth_back(0).unwrap().to_string();
        let line = line
          .trim_start_matches(prefix.as_str())
          .trim_end_matches(suffix.as_str())
          .replace("📦", "")
          .replace("🕹", "");
        Tui::writeln(
          Text::log("  ", line.as_str().trim(), "|"),
          Some(StateText::Ok(prefix.as_str())),
        );
      }
    }
  }

  pub fn stop<S: AsRef<str>>(cluster_name: S) -> AppResult<()> {
    let output = Self::mk_command(&Self {})?
      .arg("delete")
      .arg("cluster")
      .arg("--name")
      .arg(cluster_name.as_ref())
      .output()?;
    if !output.status.success() {
      return err(ErrorKind::Other, "failed to stop cluster");
    }
    Ok(())
  }
}
